#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <assert.h>
#include <string.h>
#include "common.h"
#include "ringbuf.h"
#include <semaphore.h>
#include <pthread.h>

#define REQ_SIZE 100
#define SERVER_QUEUE_DEPTH 10

void construct_request(struct buf_t *buf, int id) {
    int req_size;
    req_size = REQ_SIZE;
    assert(req_size <= MAX_REQ_SIZE);
    SET_MSG_SIZE(buf->buf, req_size);
    SET_MSG_ID(buf->buf, id);
    memset(buf->buf + MSG_START_OFFSET, 0xab, MAX_REQ_SIZE - MSG_START_OFFSET);
    buf->buf_size = req_size;
}

void print_resp(struct buf_t *buf) {
    int i;
    int id = GET_MSG_ID(buf->buf);
    printf("got response %d\n", id);
    for (i = MSG_START_OFFSET; i < buf->buf_size; i++) {
        debug_print_clean("%02x", buf->buf[i]);
        if (i % 4 == 3)
            debug_print_clean("  ");
        if (i % 16 == 15)
            debug_print_clean("\n");
    }
}

struct worker_arg_t {
    struct ringbuf_t *req_ringbuf;
};

struct sender_arg_t {
    struct ringbuf_t *req_ringbuf;
    int fd;
    sem_t *server_credits;
};

struct receiver_arg_t {
    struct ringbuf_t *resp_ringbuf;
    int fd;
    sem_t *server_credits;
};

void *worker(void *arg) {
    struct worker_arg_t *warg = (struct worker_arg_t *)arg;
    struct buf_t *buf;
    int id = 0;
    while (1) {
        buf = ringbuf_producer_get_slot(warg->req_ringbuf);
        construct_request(buf, id);
        ringbuf_producer_done(warg->req_ringbuf);
        id++;
    }
}

void *sender(void *arg) {
    struct sender_arg_t *sarg = (struct sender_arg_t *)arg;
    struct buf_t *req_buf;
    int id = 0;
    int ret;
    while (1) {
        debug_print("waiting for credits from server\n");
        sem_wait(sarg->server_credits);
        debug_print("got credits from server\n");
        req_buf = ringbuf_consumer_get_slot(sarg->req_ringbuf);
        ret = sendall(sarg->fd, req_buf->buf, req_buf->buf_size);
        SCREAM_AND_DIE_IF(ret < 0, "sendall");
        ringbuf_consumer_done(sarg->req_ringbuf);
        debug_print("sent request id=%d\n", id);
        id++;
    }
}

void *receiver(void *arg) {
    struct receiver_arg_t *rarg = (struct receiver_arg_t *)arg;
    int ret;
    struct buf_t *buf;

    while (1) {
        buf = ringbuf_producer_get_slot(rarg->resp_ringbuf);
        ret = recvall(rarg->fd, buf->buf, &buf->buf_size);
        SCREAM_AND_DIE_IF(ret < 0, "recvall");
        SCREAM_AND_DIE_IF(ret == 0, "server dead?\n");
        debug_print("got response\n");
        print_resp(buf);
        sem_post(rarg->server_credits);
        ringbuf_producer_done(rarg->resp_ringbuf);
        ringbuf_consumer_get_slot(rarg->resp_ringbuf);
        ringbuf_consumer_done(rarg->resp_ringbuf);
    }
}
int client_setup_connection(int ip, int port) {
    struct sockaddr_in saddr;
    int sfd;
    int ret;

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    SCREAM_AND_DIE_IF(sfd == -1, "socket");

    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = ip;
    ret = connect(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
    SCREAM_AND_DIE_IF(ret < 0, "connect");
    debug_print("connected to server\n");
    return sfd;
}

struct thread_pipeline_t {
    pthread_t receiver_thread;
    pthread_t sender_thread;
    pthread_t worker_thread;
    struct receiver_arg_t rarg;
    struct sender_arg_t sarg;
    struct worker_arg_t warg;
};

#define RINGBUF_SIZE 20
void create_thread_pipeline(struct thread_pipeline_t *pipeline, int fd, sem_t *server_credits) {
    struct ringbuf_t *req_ringbuf;
    struct ringbuf_t *resp_ringbuf;

    debug_print("creating ringbufs\n");
    ringbuf_create(RINGBUF_SIZE, MAX_REQ_SIZE, "client_req", &req_ringbuf);
    ringbuf_create(RINGBUF_SIZE, MAX_RESP_SIZE, "client_resp", &resp_ringbuf);

    pipeline->rarg.fd = fd;
    pipeline->sarg.fd = fd;
    pipeline->warg.req_ringbuf = req_ringbuf;
    pipeline->sarg.req_ringbuf = req_ringbuf;
    pipeline->rarg.resp_ringbuf = resp_ringbuf;
    pipeline->sarg.server_credits = server_credits;
    pipeline->rarg.server_credits = server_credits;

    debug_print("creating threads\n");
    pthread_create(&pipeline->receiver_thread, NULL, receiver, (void *)&pipeline->rarg);
    pthread_create(&pipeline->sender_thread, NULL, sender, (void *)&pipeline->sarg);
    pthread_create(&pipeline->worker_thread, NULL, worker, (void *)&pipeline->warg);
}

void wait_thread_pipeline(struct thread_pipeline_t *pipeline) {
    pthread_join(pipeline->receiver_thread, NULL);
    pthread_join(pipeline->sender_thread, NULL);
    pthread_join(pipeline->worker_thread, NULL);
    ringbuf_destroy(pipeline->warg.req_ringbuf);
    ringbuf_destroy(pipeline->rarg.resp_ringbuf);
}

#define SERVER_CREDITS_SEMAPHORE_NAME "./server_credits.sem"

int main(int argc, char *argv[]) {
    int ip;
    int port;
    int sfd;
    struct thread_pipeline_t thread_pipeline;
    sem_t *server_credits;

    if (argc < 3) {
        fprintf(stderr, "usage: %s <server ip> <server port>\n", argv[0]);
        exit(1);
    }

    ip = inet_addr(argv[1]);
    port = atoi(argv[2]);

    sfd = client_setup_connection(ip, port);

    sem_unlink(SERVER_CREDITS_SEMAPHORE_NAME);
    server_credits = sem_open(SERVER_CREDITS_SEMAPHORE_NAME, O_CREAT, 0644, SERVER_QUEUE_DEPTH);
    SCREAM_AND_DIE_IF(server_credits == SEM_FAILED, "sem_open");

    create_thread_pipeline(&thread_pipeline, sfd, server_credits);
    wait_thread_pipeline(&thread_pipeline);

    sem_close(server_credits);
    sem_unlink(SERVER_CREDITS_SEMAPHORE_NAME);

    return 0;
}
