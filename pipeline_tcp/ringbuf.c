#include "ringbuf.h"
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

struct ringbuf_t {
    struct buf_t *buf;
    int nslots;
    int pi;
    int ci;
    char sem_name_full[100];
    char sem_name_free[100];
    sem_t *sem_full;
    sem_t *sem_free;
};

void ringbuf_create(int nslots, int slot_buf_size, char *sem_name, struct ringbuf_t **rbuf) {
    int i;
    *rbuf = malloc(sizeof(struct ringbuf_t));
    struct ringbuf_t *rbufv = *rbuf;
    rbufv->pi = 0;
    rbufv->ci = 0;
    rbufv->nslots = nslots;
    sprintf(rbufv->sem_name_full, "./%s_ringbuf_full.sem", sem_name);
    sprintf(rbufv->sem_name_free, "./%s_ringbuf_free.sem", sem_name);
    sem_unlink(rbufv->sem_name_full);
    sem_unlink(rbufv->sem_name_free);
    rbufv->sem_full = sem_open(rbufv->sem_name_full, O_CREAT, 0644, 0);
    rbufv->sem_free = sem_open(rbufv->sem_name_free, O_CREAT, 0644, nslots);
    if (rbufv->sem_full == SEM_FAILED) {
        perror("sem_open(sem_full)");
        exit(1);
    }
    if (rbufv->sem_free == SEM_FAILED) {
        perror("sem_open(sem_full)");
        exit(1);
    }
    rbufv->buf = malloc(nslots * sizeof(struct buf_t));
    for (i = 0; i < nslots; i++) {
        rbufv->buf[i].buf = malloc(slot_buf_size);
        rbufv->buf[i].buf_size = 0;
    }
}

void ringbuf_destroy(struct ringbuf_t *rbuf) {
    int i;
    sem_close(rbuf->sem_full);
    sem_close(rbuf->sem_free);
    sem_unlink(rbuf->sem_name_full);
    sem_unlink(rbuf->sem_name_free);
    for (i = 0; i < rbuf->nslots; i++)
        free(rbuf->buf[i].buf);
    free(rbuf->buf);
    free(rbuf);
}

struct buf_t *ringbuf_producer_get_slot(struct ringbuf_t *rbuf) {
    struct buf_t *buf;
    sem_wait(rbuf->sem_free);
    buf = &rbuf->buf[rbuf->pi % rbuf->nslots];
    rbuf->pi++;
    return buf;
}

void ringbuf_producer_done(struct ringbuf_t *rbuf) {
    sem_post(rbuf->sem_full);
}

struct buf_t *ringbuf_consumer_get_slot(struct ringbuf_t *rbuf) {
    struct buf_t *buf;
    sem_wait(rbuf->sem_full);
    buf = &rbuf->buf[rbuf->ci % rbuf->nslots];
    rbuf->ci++;
    return buf;
}

void ringbuf_consumer_done(struct ringbuf_t *rbuf) {
    sem_post(rbuf->sem_free);
}



