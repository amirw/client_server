#ifndef __RINGBUF_H__
#define __RINGBUF_H__

#include "common.h"
#include <sys/types.h>
#include <unistd.h>

struct ringbuf_t;

struct buf_t {
    uchar *buf;
    int buf_size;
};

void ringbuf_create(int nslots, int slot_buf_size, char *sem_name, struct ringbuf_t **rbuf);
void ringbuf_destroy(struct ringbuf_t *rbuf);
struct buf_t *ringbuf_producer_get_slot(struct ringbuf_t *rbuf);
void ringbuf_producer_done(struct ringbuf_t *rbuf);
struct buf_t *ringbuf_consumer_get_slot(struct ringbuf_t *rbuf);
void ringbuf_consumer_done(struct ringbuf_t *rbuf);

#endif
