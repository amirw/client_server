#include "common.h"
#include <sys/socket.h>
#include <stdlib.h>

int sendall(int fd, uchar *buf, int size) {
    int offset = 0;
    int ret;
    while (offset < size) {
        ret = send(fd, buf + offset, size - offset, 0);
        if (ret < 0)
            return ret;
        offset += ret;
    }
    return 0;
}

int recvall(int fd, uchar *buf, int *size) {
    int ret;
    int msg_size_parsed = 0;
    int offset = 0;
    int msg_size = 0;
    while (!msg_size_parsed || (offset < msg_size)) {
        ret = recv(fd, buf, MAX_RESP_SIZE - offset, 0);
        SCREAM_AND_DIE_IF(ret <= 0, "recv");
        if (ret <= 0) {
            return ret;
        }
        offset += ret;
        if (offset > MSG_SIZE_OFFSET + MSG_SIZE_LEN) {
            msg_size_parsed = 1;
            msg_size = GET_MSG_SIZE(buf);
        }
    }
    *size = msg_size;
    return msg_size;
}
