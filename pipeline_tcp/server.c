#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include "ringbuf.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>

#define NLISTEN 10
#define SERVER_QUEUE_DEPTH 10

struct receiver_arg_t {
    int fd;
    struct ringbuf_t *req_ringbuf;
};

struct sender_arg_t {
    int fd;
    struct ringbuf_t *resp_ringbuf;
};

struct worker_arg_t {
    struct ringbuf_t *req_ringbuf;
    struct ringbuf_t *resp_ringbuf;
};

void print_req(struct buf_t *buf) {
    int i;
    int id = GET_MSG_ID(buf->buf);
    int size = buf->buf_size;
    printf("got request %d\n", id);
    for (i = MSG_START_OFFSET; i < size; i++) {
        debug_print_clean("%02x", buf->buf[i]);
        if (i % 4 == 3)
            debug_print_clean("  ");
        if (i % 16 == 15)
            debug_print_clean("\n");
    }
}

void *receiver(void *arg) {
    struct receiver_arg_t *rarg = (struct receiver_arg_t *)arg;
    struct buf_t *buf;
    int ret;
    debug_print("receiver started\n");
    while (1) {
        debug_print("requesting a producer slot\n");
        buf = ringbuf_producer_get_slot(rarg->req_ringbuf);
        debug_print("got a producer slot\n");
        ret = recvall(rarg->fd, buf->buf, &buf->buf_size);
        SCREAM_AND_DIE_IF(ret <= 0, "recvall");
        print_req(buf);
        ringbuf_producer_done(rarg->req_ringbuf);
    }
}

void *sender(void *arg) {
    struct sender_arg_t *sarg = (struct sender_arg_t *)arg;
    struct buf_t *buf;
    int ret;
    while (1) {
        debug_print("requesting a consumer slot\n");
        buf = ringbuf_consumer_get_slot(sarg->resp_ringbuf);
        debug_print("got a consumer slot\n");
        ret = sendall(sarg->fd, buf->buf, buf->buf_size);
        SCREAM_AND_DIE_IF(ret < 0, "sendall");
        ringbuf_consumer_done(sarg->resp_ringbuf);
    }
}

void create_response(struct buf_t *req_buf, struct buf_t *resp_buf) {
    int resp_size = MAX_RESP_SIZE;
    int id = GET_MSG_ID(req_buf->buf);
    SET_MSG_SIZE(resp_buf->buf, resp_size);
    SET_MSG_ID(resp_buf->buf, id);
    resp_buf->buf_size = resp_size;
    memset(resp_buf->buf + MSG_START_OFFSET, 0xcd, MAX_RESP_SIZE - MSG_START_OFFSET);
}


void *worker(void *arg) {
    struct worker_arg_t *warg = (struct worker_arg_t *)arg;
    struct buf_t *req_buf;
    struct buf_t *resp_buf;
    while (1) {
        req_buf = ringbuf_consumer_get_slot(warg->req_ringbuf);
        resp_buf = ringbuf_producer_get_slot(warg->resp_ringbuf);

        create_response(req_buf, resp_buf);

        ringbuf_consumer_done(warg->req_ringbuf);
        ringbuf_producer_done(warg->resp_ringbuf);
    }
}

int setup_server_connection(char *port) {
    struct addrinfo hints, *addrinfo_res, *addrinfo_it;
    int so_reuseaddr = 1;
    int ret;
    int lfd;
    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    ret = getaddrinfo(NULL, port, &hints, &addrinfo_res);
    SCREAM_AND_DIE_IF(ret, "getaddrinfo");

    for (addrinfo_it = addrinfo_res; addrinfo_it; addrinfo_it = addrinfo_it->ai_next) {
        lfd = socket(addrinfo_it->ai_family, addrinfo_it->ai_socktype, addrinfo_it->ai_protocol);
        if (lfd < 0)
            continue;

        setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &so_reuseaddr, sizeof(so_reuseaddr));

        ret = bind(lfd, addrinfo_it->ai_addr, addrinfo_it->ai_addrlen);
        if (ret) {
            close(lfd);
            continue;
        }
        break;
    }

    SCREAM_AND_DIE_IF(!addrinfo_it, "failed to find an address for server");
    freeaddrinfo(addrinfo_res);

    ret = listen(lfd, NLISTEN);
    SCREAM_AND_DIE_IF(ret < 0, "listen");

    return lfd;
}

struct thread_pipeline_t {
    pthread_t receiver_thread;
    pthread_t sender_thread;
    pthread_t worker_thread;
    struct receiver_arg_t rarg;
    struct sender_arg_t sarg;
    struct worker_arg_t warg;
};

void create_thread_pipeline(struct thread_pipeline_t *pipeline, int fd) {
    struct ringbuf_t *req_ringbuf;
    struct ringbuf_t *resp_ringbuf;

    debug_print("creating ringbufs\n");
    ringbuf_create(SERVER_QUEUE_DEPTH, MAX_REQ_SIZE, "server_req", &req_ringbuf);
    ringbuf_create(SERVER_QUEUE_DEPTH, MAX_RESP_SIZE, "server_resp", &resp_ringbuf);

    pipeline->rarg.fd = fd;
    pipeline->sarg.fd = fd;
    pipeline->rarg.req_ringbuf = req_ringbuf;
    pipeline->sarg.resp_ringbuf = resp_ringbuf;
    pipeline->warg.req_ringbuf = req_ringbuf;
    pipeline->warg.resp_ringbuf = resp_ringbuf;

    debug_print("creating threads\n");
    pthread_create(&pipeline->receiver_thread, NULL, receiver, (void *)&pipeline->rarg);
    pthread_create(&pipeline->sender_thread, NULL, sender, (void *)&pipeline->sarg);
    pthread_create(&pipeline->worker_thread, NULL, worker, (void *)&pipeline->warg);
}

void wait_thread_pipeline(struct thread_pipeline_t *pipeline) {
    pthread_join(pipeline->receiver_thread, NULL);
    pthread_join(pipeline->sender_thread, NULL);
    pthread_join(pipeline->worker_thread, NULL);
    ringbuf_destroy(pipeline->warg.req_ringbuf);
    ringbuf_destroy(pipeline->warg.resp_ringbuf);
}

int main(int argc, char *argv[]) {
    char *port;
    int lfd, sfd;

    struct thread_pipeline_t thread_pipeline;

    if (argc < 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }

    port = argv[1];

    lfd = setup_server_connection(port);

    while (1) {
        printf("waiting for a new connection\n");
        sfd = accept(lfd, NULL, NULL);
        SCREAM_AND_DIE_IF(sfd < 0, "accept");
        printf("got a new connection\n");

        /* TODO support multiple connections (pipeline for each new connection) */
        debug_print("will create thread pipeline\n");
        create_thread_pipeline(&thread_pipeline, sfd);
        wait_thread_pipeline(&thread_pipeline);
    }

    return 0;
}
