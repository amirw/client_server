#ifndef __COMMON_H__
#define __COMMON_H__
#include <stdio.h>

#define MAX_REQ_SIZE 100
#define MAX_RESP_SIZE 100
#define MSG_SIZE_OFFSET 0
#define MSG_SIZE_LEN sizeof(uint32_t)
#define MSG_ID_OFFSET (MSG_SIZE_OFFSET + MSG_SIZE_LEN)
#define MSG_ID_LEN sizeof(uint32_t)
#define MSG_START_OFFSET (MSG_ID_OFFSET + MSG_ID_LEN)

#define GET_MSG_SIZE(buf) ntohl(*(uint32_t *)(&buf[MSG_SIZE_OFFSET]))
#define SET_MSG_SIZE(buf, size) *(uint32_t *)(&buf[MSG_SIZE_OFFSET]) = htonl(size)
#define GET_MSG_ID(buf) ntohl(*(uint32_t *)(&buf[MSG_ID_OFFSET]))
#define SET_MSG_ID(buf, id) *(uint32_t *)(&buf[MSG_ID_OFFSET]) = htonl(id)

#define uchar unsigned char

#define SCREAM_AND_DIE_IF(cond, msg) do {\
                                         if (cond) {\
                                            perror(msg);\
                                            fflush(stderr);\
                                            exit(1);\
                                         }\
                                      } while (0)

#ifdef DEBUG
#define debug_print_clean(fmt, args...) do {\
                                fprintf(stderr, fmt, ##args);\
                                fflush(stderr);\
                             } while (0)
#define debug_print(fmt, args...) \
    debug_print_clean("%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args);
#else
#define debug_print_clean(fmt, args...)
#define debug_print(fmt, args...)
#endif

int sendall(int fd, uchar *buf, int size);
int recvall(int fd, uchar *buf, int *size);

#endif
