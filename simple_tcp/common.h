#ifndef __COMMON_H__
#define __COMMON_H__

#define MAX_REQ_SIZE 100
#define MAX_RESP_SIZE 100
#define MSG_SIZE_OFFSET 0
#define MSG_SIZE_LEN sizeof(uint32_t)

#define GET_MSG_SIZE(buf) ntohl(*(uint32_t *)(&buf[MSG_SIZE_OFFSET]))
#define SET_MSG_SIZE(buf, size) *(uint32_t *)(&buf[MSG_SIZE_OFFSET]) = htonl(size)

#define uchar unsigned char

#endif
