#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "common.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>

#define NLISTEN 10

int create_response(uchar *req, int req_size, uchar *resp) {
    int resp_size = MAX_RESP_SIZE;
    SET_MSG_SIZE(resp, resp_size);
    memset(resp + MSG_SIZE_OFFSET + MSG_SIZE_LEN, 0xcd, MAX_RESP_SIZE - MSG_SIZE_OFFSET - MSG_SIZE_LEN);
    return resp_size;
}

void print_req(uchar *buf, int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%02x", buf[i]);
        if (i % 4 == 3)
            printf("  ");
        if (i % 16 == 15)
            printf("\n");
    }
}

int main(int argc, char *argv[]) {
    char *port;
    int ret;
    int lfd, sfd;
    uchar rbuf[MAX_REQ_SIZE];
    uchar sbuf[MAX_RESP_SIZE];
    struct addrinfo hints, *addrinfo_res, *addrinfo_it;
    int so_reuseaddr = 1;
    int req_size_parsed;
    int offset;
    int req_size;
    int resp_size;
    int new_con;


    if (argc < 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }

    port = argv[1];

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    ret = getaddrinfo(NULL, port, &hints, &addrinfo_res);
    if (ret) {
        perror("getaddrinfo");
        exit(1);
    }

    for (addrinfo_it = addrinfo_res; addrinfo_it; addrinfo_it = addrinfo_it->ai_next) {
        lfd = socket(addrinfo_it->ai_family, addrinfo_it->ai_socktype, addrinfo_it->ai_protocol);
        if (lfd < 0)
            continue;

        setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &so_reuseaddr, sizeof(so_reuseaddr));

        ret = bind(lfd, addrinfo_it->ai_addr, addrinfo_it->ai_addrlen);
        if (ret) {
            close(lfd);
            continue;
        }
        break;
    }

    if (!addrinfo_it) {
        fprintf(stderr, "failed to find an address for the server\n");
        exit(1);
    }
    freeaddrinfo(addrinfo_res);

    ret = listen(lfd, NLISTEN);
    if (ret < 0) {
        perror("listen");
        exit(1);
    }

    while (1) {
        new_con = 0;
        printf("waiting for a new connection\n");
        sfd = accept(lfd, NULL, NULL);
        if (sfd < 0) {
            perror("accept");
            exit(1);
        }

        while (1) {
            offset = 0;
            req_size_parsed = 0;
            while (!req_size_parsed || (offset < req_size)) {
                ret = recv(sfd, rbuf + offset, MAX_REQ_SIZE - offset, 0);
                if (ret < 0) {
                    perror("recv");
                    exit(1);
                } else if (ret == 0) {
                    new_con = 1;
                    break;
                }
                offset += ret;
                if (offset >= MSG_SIZE_OFFSET + MSG_SIZE_LEN) {
                    req_size_parsed = 1;
                    req_size = GET_MSG_SIZE(rbuf);
                }
            }
            if (new_con)
                break;
            print_req(rbuf + MSG_SIZE_OFFSET + MSG_SIZE_LEN, req_size - MSG_SIZE_OFFSET - MSG_SIZE_LEN);

            offset = 0;
            resp_size = create_response(rbuf, req_size, sbuf);
            while (offset < resp_size) {
                ret = send(sfd, sbuf + offset, resp_size - offset, 0);
                if (ret < 0) {
                    perror("send");
                    exit(1);
                }
                offset += ret;
            }
        }
    }

    return 0;
}
