#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <assert.h>
#include <string.h>
#include "common.h"

#define REQ_SIZE 100

int construct_request(uchar *buf) {
    int req_size;
    req_size = REQ_SIZE;
    assert(req_size <= MAX_REQ_SIZE);
    SET_MSG_SIZE(buf, req_size);
    memset(buf + MSG_SIZE_OFFSET + MSG_SIZE_LEN, 0xab, MAX_REQ_SIZE - MSG_SIZE_OFFSET - MSG_SIZE_LEN);
    return req_size;
}

void print_resp(uchar *buf, int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%02x", buf[i]);
        if (i % 4 == 3)
            printf("  ");
        if (i % 16 == 15)
            printf("\n");
    }
}

int main(int argc, char *argv[]) {
    int ip;
    int port;
    int sfd;
    int ret;
    int offset;
    uchar sbuf[MAX_REQ_SIZE];
    uchar rbuf[MAX_RESP_SIZE];
    struct sockaddr_in saddr;
    int req_size;
    int resp_size;
    int resp_size_parsed;

    if (argc < 3) {
        fprintf(stderr, "usage: %s <server ip> <server port>\n", argv[0]);
        exit(1);
    }

    ip = inet_addr(argv[1]);
    port = atoi(argv[2]);

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sfd == -1) {
        perror("socket");
        exit(1);
    }

    saddr.sin_family = AF_INET;
    saddr.sin_port = htons(port);
    saddr.sin_addr.s_addr = ip;
    ret = connect(sfd, (struct sockaddr *)&saddr, sizeof(saddr));
    if (ret < 0) {
        perror("connect");
        exit(1);
    }

    while (1) {
        req_size = construct_request(sbuf);
        offset = 0;
        while (offset < req_size) {
            ret = send(sfd, sbuf + offset, req_size - offset, 0);
            if (ret < 0) {
                perror("send");
                exit(1);
            }
            offset += ret;
        }

        offset = 0;
        resp_size_parsed = 0;

        while (!resp_size_parsed || (offset < resp_size)) {
            ret = recv(sfd, rbuf + offset, MAX_RESP_SIZE - offset, 0);
            if (ret < 0) {
                perror("recv");
                exit(1);
            } else if (ret == 0) {
                fprintf(stderr, "server dead?\n");
                exit(1);
            }
            offset += ret;
            if (offset >= MSG_SIZE_OFFSET + MSG_SIZE_LEN) {
                resp_size_parsed = 1;
                resp_size = GET_MSG_SIZE(rbuf);
            }
        }
        print_resp(rbuf + MSG_SIZE_OFFSET + MSG_SIZE_LEN, resp_size - MSG_SIZE_OFFSET - MSG_SIZE_LEN);
    }
}
